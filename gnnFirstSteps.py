#from graphics.py import *
from math import *


bias= 1
numPatterns= 4
learnRate =0.01

from ast import And


w  = {0.3, -0.543, 0.122} # small random values 
xx = {0, 1, 0, 1}
yy = {0, 0, 1, 1}

#target = {0, 1, 1, 1}; # OR
target = {0, 0, 0, 1}; # AND 


def getNeuronalOutput(x,y):
    return 1.0/(1.0 + exp(-1000*(bias * w[0] + x * w[1] + y*w[2] )))


def train():
    p=0
    while p<numPatterns:
     out = getNeuronalOutput(xx[p],yy[p])
     w[0] += learnRate * (target[p]-out) * bias # deltaLernregel
     w[1] += learnRate * (target[p]-out) * xx[p] # deltaLernregel
     w[2] += learnRate * (target[p]-out) * yy[p] # deltaLernregel
    



def main(int argc, char**argv):
    openWindow((char*)"Neuron",500,500)

    while(1){
        train()
        # ---plot result
          for(double x=0; x<1;x+=0.002){
              for(double x=0; x<1;x+=0.002){
            double out= getNeuronalOutput(x,y)
            setColor((int)(out*255),(int)(out*255),(int)(out*255));
            drawPixel(x*500,500-y*500)
            }
        }
    }

main()