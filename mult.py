#!/usr/bin/python3

a = 34847592847
b = 17324759283
m = 99348759348

def multMod(a,b,m):
    result = a
    timer = 0

    for i in bin(b)[2:]:
        tempB = bin(b)[2:]
        result+=result
        timer+=1
        if(result > m):
            result -=m
            timer+=1
        if(tempB[int(i)]):
            result+=a
            timer+=1
        if(result >m):
            result-=m
            timer+=1
    
    return result,timer

result,timer = multMod(a,b,m)

print("multmod(%s, %s, %s)=%s in %s Zeiteinheiten" %  (a,b,m,result,timer))  


