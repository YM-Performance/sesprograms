#!/usr/bin/python3

from scipy.stats.stats import pearsonr

with open("RSA_timing.txt") as file:
	lines = file.readlines()
	N = int(lines[0])
	e = int(lines[1])
	ciphertexts = []
	timings = []
	for line in lines[2:]:
		c,t = line.split()
		timings.append(int(t))
		ciphertexts.append(int(c))



